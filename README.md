## Art of Rally Championship

This is the code for the art of rally championship running at https://art-of-rally-leaderboard.net.

## Contributing

Thank you for considering contributing to the Application ! You can contact me at thyseus@pm.me

## License

The Aorc is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
