<?php

namespace App\Console\Commands;

use App\Models\Car;
use App\Models\Stage;
use App\Models\Country;
use Illuminate\Console\Command;

class DataImportCommand extends Command
{
	protected $signature = 'data:import';

	protected $description = 'Import Cars and Stages';

	public function handle()
	{
		$countries = json_decode(file_get_contents(__DIR__ . '/../../../database/countries.json'));

		foreach ($countries as $country) {
			$countryModel = Country::create([
				'name' => $country->name,
				'weather_conditions' => json_encode($country->weatherList),
			]);

			foreach ($country->stageList as $stage) {
				Stage::create([
					'country_id' => $countryModel->id,
					'name' => $stage->Name,
					'is_reverse' => !$stage->IsForwardStage,
					'distance' => $stage->Distance,
					'road_surface_primary' => $stage->RoadSurfacePrimary,
					'road_surface_secondary' => $stage->RoadSurfaceSecondary,
				]);
			}
		}

		$cars = json_decode(file_get_contents(__DIR__ . '/../../../database/cars.json'));

		foreach ($cars as $car) {
			Car::create([
				'name' => $car->name,
				'class' => $car->carClass,
				'engine' => $car->carStats->Engine,
				'gears' => $car->carStats->Gears,
				'transmission' => $car->carStats->Transmission,
				'aspiration' => $car->carStats->Aspiration,
				'country' => $car->carStats->Country,
			]);
		}

		return 0;
	}
}
