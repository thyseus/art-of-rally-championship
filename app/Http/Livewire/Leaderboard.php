<?php

namespace App\Http\Livewire;

use App\Models\Tournament;
use Livewire\Component;

class Leaderboard extends Component
{
	protected ?Tournament $tournament = null;

	public function render()
	{
		$this->tournament = Tournament::where(['is_currently_running' => 1])->first();

		return view('livewire.leaderboard', ['tournament' => $this->tournament]);
	}
}
