<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stage extends Model
{
	use HasFactory;

	public $fillable = ['name', 'country_id', 'is_reverse', 'distance', 'road_surface_primary', 'road_surface_secondary'];

	public function brackets()
	{
		return $this->belongsToMany(Bracket::class);
	}
}
