<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
	use HasFactory;

	public $fillable = ['name'];

	public function brackets()
	{
		return $this->belongsToMany(Bracket::class);
	}
}
