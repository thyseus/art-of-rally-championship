<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bracket extends Model
{
	use HasFactory;

	public $fillable = ['tournament_id', 'name'];

	public function tournament()
	{
		return $this->belongsTo(Tournament::class);
	}

	public function drivers()
	{
		return $this->belongsToMany(Driver::class);
	}

	public function stages()
	{
		return $this->belongsToMany(Stage::class);
	}
}
