<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tournament extends Model
{
	use HasFactory;

	public $fillable = ['name', 'start_scheduled_at', 'is_currently_running'];

	public function brackets()
	{
		return $this->hasMany(Bracket::class);
	}

	public static function next(): ?Tournament
	{
		return Tournament::query()->orderBy('start_scheduled_at')->first();
	}

	public function timeFor(Driver $driver, Stage $stage): string
	{
		return Time::query()->where(
			[
				'tournament_id' => $this->id,
				'driver_id' => $driver->id,
				'stage_id' => $stage->id,
			]
		)->first()->time ?? ' - ';
	}
}
