<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Time extends Model
{
	use HasFactory;

	public $fillable = ['tournament_id', 'driver_id', 'stage_id', 'car_id', 'livery_id', 'time'];

	public function driver()
	{
		return $this->belongsTo(Driver::class);
	}

	public function stage()
	{
		return $this->belongsTo(Stage::class);
	}

	public function car()
	{
		return $this->belongsTo(Car::class);
	}

	public function livery()
	{
		return $this->belongsTo(Livery::class);
	}

	public function tournament()
	{
		return $this->belongsTo(Tournament::class);
	}
}
