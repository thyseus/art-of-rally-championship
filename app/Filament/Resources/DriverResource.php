<?php

namespace App\Filament\Resources;

use App\Filament\Resources\DriverResource\Pages;
use App\Models\Driver;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;

class DriverResource extends Resource
{
	protected static ?string $model = Driver::class;

	protected static ?string $navigationIcon = 'heroicon-s-user';

	protected static ?string $recordTitleAttribute = 'name';

	public static function form(Form $form): Form
	{
		return $form
			->schema([
				Forms\Components\TextInput::make('name')
					->required()
					->maxLength(255),
			]);
	}

	public static function table(Table $table): Table
	{
		return $table
			->columns([
				Tables\Columns\TextColumn::make('created_at')
					->dateTime(),
				Tables\Columns\TextColumn::make('updated_at')
					->dateTime(),
				Tables\Columns\TextColumn::make('name'),
			])
			->filters([
			]);
	}

	public static function getRelations(): array
	{
		return [
		];
	}

	public static function getPages(): array
	{
		return [
			'index' => Pages\ListDrivers::route('/'),
			'create' => Pages\CreateDriver::route('/create'),
			'edit' => Pages\EditDriver::route('/{record}/edit'),
		];
	}
}
