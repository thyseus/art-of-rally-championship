<?php

namespace App\Filament\Resources;

use App\Filament\Resources\TournamentResource\Pages;
use App\Filament\Resources\TournamentResource\RelationManagers\BracketsRelationManager;
use App\Models\Tournament;
use Filament\Forms\Components\DateTimePicker;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Toggle;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables\Columns\TextColumn;

class TournamentResource extends Resource
{
	protected static ?string $model = Tournament::class;

	protected static ?string $navigationIcon = 'heroicon-o-collection';

	protected static ?string $recordTitleAttribute = 'name';

	public static function form(Form $form): Form
	{
		return $form
			->schema([
				TextInput::make('name')->required(),
				DateTimePicker::make('start_scheduled_at')->required(),
				Toggle::make('is_currently_running')->required(),
			]);
	}

	public static function table(Table $table): Table
	{
		return $table
			->columns([
				TextColumn::make('name')->sortable(),
			])
			->filters([
			]);
	}

	public static function getRelations(): array
	{
		return [
			BracketsRelationManager::class,
		];
	}

	public static function getPages(): array
	{
		return [
			'index' => Pages\ListTournaments::route('/'),
			'create' => Pages\CreateTournament::route('/create'),
			'edit' => Pages\EditTournament::route('/{record}/edit'),
		];
	}
}
