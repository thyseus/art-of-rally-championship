<?php

namespace App\Filament\Resources\BracketResource\Pages;

use App\Filament\Resources\BracketResource;
use Filament\Resources\Pages\ListRecords;

class ListBrackets extends ListRecords
{
    protected static string $resource = BracketResource::class;
}
