<?php

namespace App\Filament\Resources\BracketResource\Pages;

use App\Filament\Resources\BracketResource;
use Filament\Resources\Pages\EditRecord;

class EditBracket extends EditRecord
{
    protected static string $resource = BracketResource::class;
}
