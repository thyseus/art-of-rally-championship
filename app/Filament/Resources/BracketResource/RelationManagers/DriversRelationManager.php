<?php

namespace App\Filament\Resources\BracketResource\RelationManagers;

use App\Models\Driver;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\RelationManagers\BelongsToManyRelationManager;
use Filament\Resources\Table;
use Filament\Tables\Columns\TextColumn;

class DriversRelationManager extends BelongsToManyRelationManager
{
	protected static string $relationship = 'drivers';

	protected static ?string $recordTitleAttribute = 'name';

	public static function form(Form $form): Form
	{
		return $form
			->schema([
				TextInput::make('name'),
			]);
	}

	public static function table(Table $table): Table
	{
		return $table
			->columns([
				TextColumn::make('name')
					->url(fn (Driver $record): string => route('filament.resources.drivers.edit', ['record' => $record])),
			])
			->filters([
			]);
	}
}
