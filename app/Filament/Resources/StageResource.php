<?php

namespace App\Filament\Resources;

use App\Filament\Resources\StageResource\Pages;
use App\Models\Country;
use App\Models\Stage;
use Filament\Forms;
use Filament\Forms\Components\Select;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables\Columns\TextColumn;

class StageResource extends Resource
{
	protected static ?string $model = Stage::class;

	protected static ?string $navigationIcon = 'heroicon-o-arrow-up';

	protected static ?string $recordTitleAttribute = 'name';

	public static function form(Form $form): Form
	{
		return $form
			->schema([
				Forms\Components\TextInput::make('name')
					->required()
					->maxLength(255),

				Select::make('country_id')
					->required()
					->label('Country')
					->options(Country::all()->pluck('name', 'id'))
					->searchable(),
			]);
	}

	public static function table(Table $table): Table
	{
		return $table
			->columns([
				TextColumn::make('created_at')
					->dateTime(),
				TextColumn::make('name'),
				TextColumn::make('country'),
			])
			->filters([
			]);
	}

	public static function getRelations(): array
	{
		return [
		];
	}

	public static function getPages(): array
	{
		return [
			'index' => Pages\ListStages::route('/'),
			'create' => Pages\CreateStage::route('/create'),
			'edit' => Pages\EditStage::route('/{record}/edit'),
		];
	}
}
