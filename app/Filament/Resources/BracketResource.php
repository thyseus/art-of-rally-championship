<?php

namespace App\Filament\Resources;

use App\Filament\Resources\BracketResource\Pages;
use App\Filament\Resources\BracketResource\RelationManagers\DriversRelationManager;
use App\Filament\Resources\StageResource\RelationManagers\StagesRelationManager;
use App\Models\Bracket;
use App\Models\Tournament;
use Filament\Forms;
use Filament\Forms\Components\Select;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;

class BracketResource extends Resource
{
	protected static ?string $model = Bracket::class;

	protected static ?string $navigationIcon = 'heroicon-o-collection';

	protected static ?string $recordTitleAttribute = 'name';

	public static function form(Form $form): Form
	{
		return $form
			->schema([
				Select::make('tournament_id')
					->required()
					->label('Tournament')
					->options(Tournament::all()->pluck('name', 'id'))
					->searchable(),

				Forms\Components\TextInput::make('name')
					->required()
					->maxLength(255),
			]);
	}

	public static function table(Table $table): Table
	{
		return $table
			->columns([
				Tables\Columns\TextColumn::make('created_at')
					->dateTime(),
				Tables\Columns\TextColumn::make('updated_at')
					->dateTime(),
				Tables\Columns\TextColumn::make('tournament.name'),
				Tables\Columns\TextColumn::make('name'),
			])
			->filters([
			]);
	}

	public static function getRelations(): array
	{
		return [
			DriversRelationManager::class,
			StagesRelationManager::class,
		];
	}

	public static function getPages(): array
	{
		return [
			'index' => Pages\ListBrackets::route('/'),
			'create' => Pages\CreateBracket::route('/create'),
			'edit' => Pages\EditBracket::route('/{record}/edit'),
		];
	}
}
