<?php

namespace App\Filament\Resources\TournamentResource\RelationManagers;

use App\Models\Bracket;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\RelationManagers\HasManyRelationManager;
use Filament\Resources\Table;
use Filament\Tables\Columns\TextColumn;

class BracketsRelationManager extends HasManyRelationManager
{
	protected static string $relationship = 'brackets';

	protected static ?string $recordTitleAttribute = 'name';

	public static function form(Form $form): Form
	{
		return $form
			->schema([
				TextInput::make('name')->required(),
			]);
	}

	public static function table(Table $table): Table
	{
		return $table
			->columns([
				TextColumn::make('name')
					->url(fn (Bracket $record): string => route('filament.resources.brackets.edit', ['record' => $record])),
			])
			->filters([
			]);
	}
}
