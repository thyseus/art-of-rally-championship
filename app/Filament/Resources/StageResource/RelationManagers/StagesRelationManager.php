<?php

namespace App\Filament\Resources\StageResource\RelationManagers;

use App\Models\Stage;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\RelationManagers\BelongsToManyRelationManager;
use Filament\Resources\Table;
use Filament\Tables\Columns\TextColumn;

class StagesRelationManager extends BelongsToManyRelationManager
{
	protected static string $relationship = 'stages';

	protected static ?string $recordTitleAttribute = 'name';

	public static function form(Form $form): Form
	{
		return $form
			->schema([
				Forms\Components\TextInput::make('name')
					->required()
					->maxLength(255),
				Forms\Components\TextInput::make('country')
					->required()
					->maxLength(255),
			]);
	}

	public static function table(Table $table): Table
	{
		return $table
			->columns([
				TextColumn::make('created_at')
					->dateTime(),
				TextColumn::make('updated_at')
					->dateTime(),
				TextColumn::make('name')
					->url(fn (Stage $record): string => route('filament.resources.stages.edit', ['record' => $record])),
			])
			->filters([
			]);
	}
}
