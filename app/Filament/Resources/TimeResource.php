<?php

namespace App\Filament\Resources;

use App\Filament\Resources\TimeResource\Pages;
use App\Models\Car;
use App\Models\Livery;
use App\Models\Driver;
use App\Models\Stage;
use App\Models\Time;
use App\Models\Tournament;
use Filament\Forms;
use Filament\Forms\Components\Select;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;

class TimeResource extends Resource
{
	protected static ?string $model = Time::class;

	protected static ?string $navigationIcon = 'heroicon-o-clock';

	public static function form(Form $form): Form
	{
		return $form
			->schema([
				Select::make('tournament_id')
					->required()
					->label('Tournament')
					->options(Tournament::all()->pluck('name', 'id'))
					->searchable(),
				Select::make('driver_id')
					->required()
					->label('Driver')
					->options(Driver::all()->pluck('name', 'id'))
					->searchable(),
				Select::make('stage_id')
					->required()
					->label('Stage')
					->options(Stage::all()->pluck('name', 'id'))
					->searchable(),
				Select::make('car_id')
					->required()
					->label('Car')
					->options(Car::all()->pluck('name', 'id'))
					->searchable(),
				Select::make('livery_id')
					->required()
					->label('Livery')
					->options(Livery::all()->pluck('name', 'id'))
					->searchable(),
				Forms\Components\TextInput::make('time')
					->required(),
			]);
	}

	public static function table(Table $table): Table
	{
		return $table
			->columns([
				Tables\Columns\TextColumn::make('created_at')->dateTime(),
				Tables\Columns\TextColumn::make('updated_at')->dateTime(),
				Tables\Columns\TextColumn::make('tournament.name'),
				Tables\Columns\TextColumn::make('driver.name'),
				Tables\Columns\TextColumn::make('stage.name'),
				Tables\Columns\TextColumn::make('car.name'),
				Tables\Columns\TextColumn::make('livery.name'),
				Tables\Columns\TextColumn::make('time'),
			])
			->filters([
			]);
	}

	public static function getRelations(): array
	{
		return [
		];
	}

	public static function getPages(): array
	{
		return [
			'index' => Pages\ListTimes::route('/'),
			'create' => Pages\CreateTime::route('/create'),
			'edit' => Pages\EditTime::route('/{record}/edit'),
		];
	}
}
