<?php

namespace App\Filament\Resources;

use App\Filament\Resources\CarResource\Pages;
use App\Models\Car;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;

class CarResource extends Resource
{
	protected static ?string $model = Car::class;

	protected static ?string $navigationIcon = 'heroicon-o-collection';

	protected static ?string $recordTitleAttribute = 'name';

	public static function form(Form $form): Form
	{
		return $form
			->schema([
				Forms\Components\TextInput::make('name')
					->required()
					->maxLength(255),
				Forms\Components\TextInput::make('class')
					->required()
					->maxLength(255),
				Forms\Components\TextInput::make('transmission')
					->required()
					->maxLength(255),
				Forms\Components\TextInput::make('gears')
					->required()
					->maxLength(255),
				Forms\Components\TextInput::make('aspiration')
					->required()
					->maxLength(255),
				Forms\Components\TextInput::make('country')
					->required()
					->maxLength(255),
				Forms\Components\TextInput::make('engine')
					->required()
					->maxLength(255),
			]);
	}

	public static function table(Table $table): Table
	{
		return $table
			->columns([
				Tables\Columns\TextColumn::make('created_at')
					->dateTime(),
				Tables\Columns\TextColumn::make('updated_at')
					->dateTime(),
				Tables\Columns\TextColumn::make('name'),
				Tables\Columns\TextColumn::make('class'),
				Tables\Columns\TextColumn::make('transmission'),
				Tables\Columns\TextColumn::make('gears'),
				Tables\Columns\TextColumn::make('aspiration'),
				Tables\Columns\TextColumn::make('country'),
				Tables\Columns\TextColumn::make('engine'),
			])
			->filters([
			]);
	}

	public static function getRelations(): array
	{
		return [
		];
	}

	public static function getPages(): array
	{
		return [
			'index' => Pages\ListCars::route('/'),
			'create' => Pages\CreateCar::route('/create'),
			'edit' => Pages\EditCar::route('/{record}/edit'),
		];
	}
}
