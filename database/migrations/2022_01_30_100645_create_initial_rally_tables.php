<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInitialRallyTables extends Migration
{
	/**
	 * Run the migrations.
	 */
	public function up()
	{
		Schema::create('countries', function (Blueprint $table) {
			$table->id();
			$table->timestamps();
			$table->string('name');
			$table->string('weather_conditions');
		});

		Schema::create('stages', function (Blueprint $table) {
			$table->id();
			$table->timestamps();
			$table->integer('country_id');
			$table->boolean('is_reverse');
			$table->string('name');
			$table->string('distance');
			$table->string('road_surface_primary');
			$table->string('road_surface_secondary');
		});

		Schema::create('drivers', function (Blueprint $table) {
			$table->id();
			$table->timestamps();
			$table->string('name');
		});

		Schema::create('cars', function (Blueprint $table) {
			$table->id();
			$table->timestamps();
			$table->string('name');
			$table->string('class');
			$table->string('transmission');
			$table->string('gears');
			$table->string('aspiration');
			$table->string('country');
			$table->string('engine');
		});

		Schema::create('liveries', function (Blueprint $table) {
			$table->id();
			$table->timestamps();
			$table->string('name');
		});

		Schema::create('times', function (Blueprint $table) {
			$table->id();
			$table->timestamps();
			$table->integer('tournament_id')->nullable();
			$table->integer('driver_id');
			$table->integer('stage_id');
			$table->integer('car_id');
			$table->integer('livery_id');
			$table->decimal('time', 6, 3)->comment('In seconds');
		});

		Schema::create('tournaments', function (Blueprint $table) {
			$table->id();
			$table->timestamps();
			$table->string('name');
			$table->datetime('start_scheduled_at');
			$table->boolean('is_currently_running')->default(0);
		});

		Schema::create('brackets', function (Blueprint $table) {
			$table->id();
			$table->timestamps();
			$table->integer('tournament_id');
			$table->string('name');
		});

		Schema::create('bracket_stage', function (Blueprint $table) {
			$table->id();
			$table->timestamps();
			$table->integer('bracket_id');
			$table->integer('stage_id');
		});

		Schema::create('bracket_driver', function (Blueprint $table) {
			$table->id();
			$table->timestamps();
			$table->integer('bracket_id');
			$table->integer('driver_id');
		});
	}

	/**
	 * Reverse the migrations.
	 */
	public function down()
	{
		Schema::dropIfExists('stages');
	}
}
