MAKEFLAGS=-s

help:
	echo "Possible commands are:\n"
	cat ${MAKEFILE_LIST} | grep -v "{MAKEFILE_LIST} | grep -v" | grep ":.*##" | column -t -s "##"

install: ## install hall-of-records
	touch .env
	cp .env .env.orig
	cp .env.example .env
	docker run --rm -u "$$(id -u):$$(id -g)" -v $$(pwd):/var/www/html -w /var/www/html composer:2 composer install --ignore-platform-reqs
	./vendor/bin/sail down
	./vendor/bin/sail up -d
	./vendor/bin/sail artisan key:generate
	./vendor/bin/sail npm install
	./vendor/bin/sail artisan migrate:fresh
	./vendor/bin/sail artisan db:seed --class=Database\\Seeders\\AdminSeeder
	./vendor/bin/sail artisan db:seed --class=Database\\Seeders\\DemoDataSeeder
	${MAKE} create_test_database
	./vendor/bin/sail restart workspace
	wget https://github.com/dg/ftp-deployment/releases/download/v3.5.1/deployment.phar

create_test_database: ## create the test databases on mariadb. Should only run once.
	docker exec art-of-rally-championship_test mysql -uroot -ppassword -hmariadb art_of_rally_championship -e "create database art_of_rally_championship_test" || true

test: ## migrate fresh and run all tests
	vendor/bin/sail artisan test

deploy: ## deploy to art-of-rally-championship.net Requires FTP_REMOTE environment variable.
	composer install --no-dev
	npm install
	npm run prod
	php ./deployment.phar ${PWD}/deployment.php
