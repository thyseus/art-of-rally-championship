<div wire:poll>
    <div class="ml-4 text-sm text-center text-gray-900 sm:ml-0">
        @if ($tournament)
            @foreach ($tournament->brackets as $bracket)
                <h2> Bracket: {{ $bracket->name }} </h2>
                <table class="table-auto stripe hover">
                    <tr>
                        <th> Driver </th>
                        <th> Car </th>
                        <th> Interval </th>
                        @foreach ($bracket->stages as $stage)
                            <th> {{ $stage->name }} </th>
                        @endforeach
                    </tr>
                    @foreach ($bracket->drivers as $driver)
                        <tr>
                            <td> {{ $driver->name }} </td>
                            <td> - </td>
                            @foreach ($bracket->stages as $stage)
                                <th> {{ $tournament->timeFor($driver, $stage) }} </th>
                            @endforeach
                        </tr>
                    @endforeach
                </table>
            @endforeach
        @else
            <p> Currently there is no tournament running.
                @if ($next = App\Models\Tournament::next())
                    The next Tournament is scheduled at: {{ $next->start_scheduled_at }} </p>
                @else
                    There is no tournament scheduled yet. Stay tuned !
                @endif
                </p>
            @endif
    </div>
</div>
